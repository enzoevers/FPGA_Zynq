In this repository all the projects including FPGA and or Zynq devices are put here. 
If a project only used one kind if FPGA/Zynq the project is put in the folder with the name of the board.
Otherwise, if more kinds of FPGA/Zynq devices are used the project is put in the the folder of the project name. 

Demo video's can be found on my [youtube channel](https://www.youtube.com/playlist?list=PLDeSwiieRghTiXGDtf0CMUrUnGnAqco5Z).

## Learing Zynq
If you are new to Zynq I suggest following the tutorials on the [ZynqBook](http://www.zynqbook.com/).
Be aware that the book has tutorials for both the Zybo and the Zed board.

If you are looking to use Python for Zynq development get the Pynq board.
The [Pynq website](http://www.pynq.io/) has a lot of good qualitity tutorials and examples.

## Learning FPGA
Embedded Micro makes the Mojo V3. Their site has a lot of [quality tutorials](https://embeddedmicro.com/tutorials).