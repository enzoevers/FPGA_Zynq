// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2017.2
// Copyright (C) 1986-2017 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

(* CORE_GENERATION_INFO="Invert,hls_ip_2017_2,{HLS_INPUT_TYPE=cxx,HLS_INPUT_FLOAT=0,HLS_INPUT_FIXED=1,HLS_INPUT_PART=xc7z010clg400-1,HLS_INPUT_CLOCK=10.000000,HLS_INPUT_ARCH=others,HLS_SYN_CLOCK=2.070000,HLS_SYN_LAT=0,HLS_SYN_TPT=none,HLS_SYN_MEM=0,HLS_SYN_DSP=0,HLS_SYN_FF=0,HLS_SYN_LUT=2}" *)

module Invert (
        ap_start,
        ap_done,
        ap_idle,
        ap_ready,
        value_V_i,
        value_V_o,
        value_V_o_ap_vld
);


input   ap_start;
output   ap_done;
output   ap_idle;
output   ap_ready;
input  [0:0] value_V_i;
output  [0:0] value_V_o;
output   value_V_o_ap_vld;

reg value_V_o_ap_vld;

always @ (*) begin
    if ((ap_start == 1'b1)) begin
        value_V_o_ap_vld = 1'b1;
    end else begin
        value_V_o_ap_vld = 1'b0;
    end
end

assign ap_done = ap_start;

assign ap_idle = 1'b1;

assign ap_ready = ap_start;

assign value_V_o = (value_V_i ^ 1'd1);

endmodule //Invert
