
log_wave -r /
set designtopgroup [add_wave_group "Design Top Signals"]
set cinoutgroup [add_wave_group "C InOuts" -into $designtopgroup]
set value_group [add_wave_group value(wire) -into $cinoutgroup]
add_wave /apatb_Invert_top/AESL_inst_Invert/value_V_o_ap_vld -into $value_group -color #ffff00 -radix hex
add_wave /apatb_Invert_top/AESL_inst_Invert/value_V_o -into $value_group -radix hex
add_wave /apatb_Invert_top/AESL_inst_Invert/value_V_i -into $value_group -radix hex
set blocksiggroup [add_wave_group "Block-level IO Handshake" -into $designtopgroup]
add_wave /apatb_Invert_top/AESL_inst_Invert/ap_start -into $blocksiggroup
add_wave /apatb_Invert_top/AESL_inst_Invert/ap_done -into $blocksiggroup
add_wave /apatb_Invert_top/AESL_inst_Invert/ap_idle -into $blocksiggroup
add_wave /apatb_Invert_top/AESL_inst_Invert/ap_ready -into $blocksiggroup
set resetgroup [add_wave_group "Reset" -into $designtopgroup]
set clockgroup [add_wave_group "Clock" -into $designtopgroup]
set testbenchgroup [add_wave_group "Test Bench Signals"]
set tbinternalsiggroup [add_wave_group "Internal Signals" -into $testbenchgroup]
set tb_simstatus_group [add_wave_group "Simulation Status" -into $tbinternalsiggroup]
set tb_portdepth_group [add_wave_group "Port Depth" -into $tbinternalsiggroup]
add_wave /apatb_Invert_top/AUTOTB_TRANSACTION_NUM -into $tb_simstatus_group -radix hex
add_wave /apatb_Invert_top/ready_cnt -into $tb_simstatus_group -radix hex
add_wave /apatb_Invert_top/done_cnt -into $tb_simstatus_group -radix hex
add_wave /apatb_Invert_top/LENGTH_value_V -into $tb_portdepth_group -radix hex
set tbcinoutgroup [add_wave_group "C InOuts" -into $testbenchgroup]
set tb_value_group [add_wave_group value(wire) -into $tbcinoutgroup]
add_wave /apatb_Invert_top/value_V_o_ap_vld -into $tb_value_group -color #ffff00 -radix hex
add_wave /apatb_Invert_top/value_V_o -into $tb_value_group -radix hex
add_wave /apatb_Invert_top/value_V_i -into $tb_value_group -radix hex
save_wave_config Invert.wcfg
run all
quit

