############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2017 Xilinx, Inc. All Rights Reserved.
############################################################
open_project Inverter
set_top Invert
add_files Sources/Inverter.cpp
add_files -tb Sources/Inverter_tb.cpp
open_solution "solution2"
set_part {xc7z010clg400-1} -tool vivado
create_clock -period 10 -name default
source "./Inverter/solution2/directives.tcl"
csim_design -clean
csynth_design
cosim_design -trace_level all -tool xsim
export_design -rtl verilog -format ip_catalog -vendor "Enzo" -display_name "hls_Inverter_axilite"
