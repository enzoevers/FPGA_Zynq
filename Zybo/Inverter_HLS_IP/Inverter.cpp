#include "Inverter.h"
#include "ap_int.h"

void Invert(ap_uint<1>& value)
{
	value = ~value;
}
