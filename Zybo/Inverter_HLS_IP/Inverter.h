#ifndef _INVERTER_H_
#define _INVERTER_H_

#include "ap_int.h"

void Invert(ap_uint<1>& value);

#endif // _INVERTER_H_