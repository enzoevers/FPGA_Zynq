#include <iostream>
#include "ap_int.h"
#include "Inverter.h"

int main(int argc, char **argv)
{
	ap_uint<1> myVal = 1;

	bool test_1 = false;
	std::cout <<  "myVal was " << myVal << std::endl;
	Invert(myVal);
	std::cout <<  "myVal = " << myVal << std::endl;
	test_1 = myVal == 0;

	bool test_2 = false;
	myVal = 0;
	std::cout <<  "myVal was " << myVal << std::endl;
	Invert(myVal);
	std::cout <<  "myVal = " << myVal << std::endl;
	test_2 = myVal == 1;

	if(test_1 && test_2)
	{
		std::cout << "[TEST PASSED}" << std::endl;
	}
	else
	{
		std::cout << "[TEST FAILED}" << std::endl;
	}

	return 0;
}
