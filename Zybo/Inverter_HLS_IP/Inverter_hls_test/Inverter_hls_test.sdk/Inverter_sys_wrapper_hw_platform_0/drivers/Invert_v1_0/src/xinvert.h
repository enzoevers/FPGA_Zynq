// ==============================================================
// File generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2017.2
// Copyright (C) 1986-2017 Xilinx, Inc. All Rights Reserved.
// 
// ==============================================================

#ifndef XINVERT_H
#define XINVERT_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "xinvert_hw.h"

/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
#else
typedef struct {
    u16 DeviceId;
    u32 Axilites_BaseAddress;
} XInvert_Config;
#endif

typedef struct {
    u32 Axilites_BaseAddress;
    u32 IsReady;
} XInvert;

/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define XInvert_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define XInvert_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define XInvert_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define XInvert_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif

/************************** Function Prototypes *****************************/
#ifndef __linux__
int XInvert_Initialize(XInvert *InstancePtr, u16 DeviceId);
XInvert_Config* XInvert_LookupConfig(u16 DeviceId);
int XInvert_CfgInitialize(XInvert *InstancePtr, XInvert_Config *ConfigPtr);
#else
int XInvert_Initialize(XInvert *InstancePtr, const char* InstanceName);
int XInvert_Release(XInvert *InstancePtr);
#endif


void XInvert_Set_value_V_i(XInvert *InstancePtr, u32 Data);
u32 XInvert_Get_value_V_i(XInvert *InstancePtr);
u32 XInvert_Get_value_V_o(XInvert *InstancePtr);
u32 XInvert_Get_value_V_o_vld(XInvert *InstancePtr);

#ifdef __cplusplus
}
#endif

#endif
