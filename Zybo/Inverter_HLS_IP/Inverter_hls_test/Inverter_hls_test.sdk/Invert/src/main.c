/*
 * main.c
 *
 *  Created on: Aug 22, 2017
 *      Author: enzoevers
 */

#include <xparameters.h>
#include <xil_printf.h>

#include "xinvert.h"
#include "xil_types.h"

#define INVERT_ID XPAR_INVERT_0_DEVICE_ID

XInvert Invert;

int main()
{
	XInvert_Config* invert_cnfg_P = XInvert_LookupConfig(INVERT_ID);
	if (!invert_cnfg_P)
	{
		xil_printf("ERROR: Lookup of INVERT configuration failed.\n\r");
	}

	int status = XST_SUCCESS;

	status = XInvert_CfgInitialize(&Invert, invert_cnfg_P);
	if (status != XST_SUCCESS)
	{
		xil_printf("ERROR: Could not initialize INVERT.\n\r");
	}

	xil_printf("default XInvert_Get_value_V_i = %d\n\r", XInvert_Get_value_V_i(&Invert));
	xil_printf("default XInvert_Get_value_V_o = %d\n\r", XInvert_Get_value_V_o(&Invert));

	u32 myVal = 1;
	xil_printf("Original myVal = %d\n\r", myVal);

	XInvert_Set_value_V_i(&Invert, myVal);
	myVal = XInvert_Get_value_V_o(&Invert);

	xil_printf("Inverted myVal = %d\n\r", myVal);

	XInvert_Set_value_V_i(&Invert, myVal);
	myVal = XInvert_Get_value_V_o(&Invert);

	xil_printf("Inverting this again gives myVal = %d\n\r", myVal);

	return 0;
}
