/*
 * main.c
 *
 *  Created on: Jul 10, 2017
 *      Author: enzoevers
 */

//----------------------------------------------------------//
//			Includes										//
//----------------------------------------------------------//
#include "xparameters.h"
#include "xgpio.h"

//----------------------------------------------------------//
//			Defines											//
//----------------------------------------------------------//
#define GPIO_OUT 		XPAR_AXI_GPIO_0_DEVICE_ID	// Found in xparameters.h
#define GPIO_IN			XPAR_AXI_GPIO_1_DEVICE_ID	// Found in xparameters.h

#define LED_CHANNEL		1	// axi_gpio_0 has one channel (GPIO)
#define BTNS_CHANNEL	1	// axi_gpio_1 first channel (GPIO) is connected to the btns_4bits
#define SWS_CHANNEL		2	// axi_gpio_1 second channel (GPIO2) is connected to the sws_4bits

#define DIR_OUT			0x0 // 0b0000
#define DIR_IN			0xf // 0bffff

//----------------------------------------------------------//
//			Global variables								//
//----------------------------------------------------------//
XGpio gpio_out, gpio_in;

//----------------------------------------------------------//
//			Function prototypes								//
//----------------------------------------------------------//
u8 Init_GPIO();

//==========================================================//
//			Main											//
//==========================================================//
int main()
{
	Init_GPIO();

	u8 Btn_State = 0x0; // Default value
	u8 Sws_State = 0x0; // Default value

	u8 LEDs_data = 0x0; // Default value

	while(1)
	{
		Btn_State = XGpio_DiscreteRead(&gpio_in, BTNS_CHANNEL);
		Sws_State = XGpio_DiscreteRead(&gpio_in, SWS_CHANNEL);

		LEDs_data = Btn_State ^ Sws_State; // Binary XOR Btn_State and Sws_State

		XGpio_DiscreteWrite(&gpio_out, LED_CHANNEL, LEDs_data);
	}

	return 0;
}

//----------------------------------------------------------//
//			Function implementation							//
//----------------------------------------------------------//
u8 Init_GPIO()
{
	u8 status = XST_FAILURE;

	// Initialize axi_gpio_0
	status = XGpio_Initialize(&gpio_out, GPIO_OUT);
	if(status != XST_SUCCESS)
	{
		return status;
	}
	XGpio_SetDataDirection(&gpio_out, LED_CHANNEL, DIR_OUT);

	// Initialize axi_gpio_1
	status = XGpio_Initialize(&gpio_in, GPIO_IN);
	if(status != XST_SUCCESS)
	{
		return status;
	}
	XGpio_SetDataDirection(&gpio_in, BTNS_CHANNEL, DIR_IN);
	XGpio_SetDataDirection(&gpio_in, SWS_CHANNEL, DIR_IN);

	return status;
}
