/*
 * SSM2603.c
 *
 *  Created on: Jul 9, 2017
 *      Author: enzoevers
 */

#include "SSM2603.h"
#include "I2C.h"
#include "GPIO.h"
#include "sleep.h"

// The MUTE signal
#define AUDIO_ENABLE_ID			XPAR_AXI_GPIO_0_DEVICE_ID
#define AUDIO_ENABLE_CHANNEL	1
XGpio Gpio_audio_enable;

u8 Init_SSM2603()
{
	Init_SSM2603_reg();
	return Init_SSM2603_AudioEnable();
}

void Init_SSM2603_reg()
{
	W_I2C(I2C_SLAVE_ADDR, R15_SOFTWARE_RESET, 					0b000000000); //Perform Reset
	usleep(75000);
	W_I2C(I2C_SLAVE_ADDR, R6_POWER_MANAGEMENT, 					0b000110000); //Power Up
	W_I2C(I2C_SLAVE_ADDR, R0_LEFT_CHANNEL_ADC_INPUT_VOLUME, 	0b000010111); //Default Volume
	W_I2C(I2C_SLAVE_ADDR, R1_RIGHT_CHANNEL_ADC_INPUT_VOLUME, 	0b000010111); //Default Volume
	W_I2C(I2C_SLAVE_ADDR, R2_LEFT_CHANNEL_DAC_VOLUME, 			0b001111001);
	W_I2C(I2C_SLAVE_ADDR, R3_RIGHT_CHANNEL_DAC_VOLUME, 			0b001111001);
	W_I2C(I2C_SLAVE_ADDR, R4_ANALOG_AUDIO_PATH, 				0b000010010); //Allow Mixed DAC, Mute MIC
	W_I2C(I2C_SLAVE_ADDR, R5_DIGITAL_AUDIO_PATH, 				0b000000111); //48 kHz Sampling Rate emphasis, no high pass
	W_I2C(I2C_SLAVE_ADDR, R7_DIGITAL_AUDIO_I_F, 				0b000001110); //I2S Mode, set-up 32 bits
	W_I2C(I2C_SLAVE_ADDR, R8_SAMPLING_RATE, 					0b000000000);
	usleep(75000);
	W_I2C(I2C_SLAVE_ADDR, R9_ACTIVE, 							0b000000001); //Activate digital core
	W_I2C(I2C_SLAVE_ADDR, R6_POWER_MANAGEMENT, 					0b000100010); //Output Power Up
}

u8 Init_SSM2603_AudioEnable()
{
	int status = XGpio_Initialize(&Gpio_audio_enable, AUDIO_ENABLE_ID);
	if(status != XST_SUCCESS)
	{
		status = XST_FAILURE;
	}

	XGpio_SetDataDirection(&Gpio_audio_enable, AUDIO_ENABLE_CHANNEL, 0x0); // Set direction as output

	return XST_SUCCESS;
}

void Stream_LineIn_HphOut()
{
	u32  in_left, in_right;

	while(R_SWS() != 0b111)
	{
		W_LEDS(R_SWS());

		// Read audio input from codec
		in_left = Xil_In32(I2S_DATA_RX_L_REG);
		in_right = Xil_In32(I2S_DATA_RX_R_REG);

		// Write audio input to codec
		Xil_Out32(I2S_DATA_TX_L_REG, in_left);
		Xil_Out32(I2S_DATA_TX_R_REG, in_right);
	}
}

void SSM2603_Enable()
{
	XGpio_DiscreteWrite(&Gpio_audio_enable, AUDIO_ENABLE_CHANNEL, 1);
}

void SSM2603_Disable()
{
	XGpio_DiscreteWrite(&Gpio_audio_enable, AUDIO_ENABLE_CHANNEL, 0);
}
