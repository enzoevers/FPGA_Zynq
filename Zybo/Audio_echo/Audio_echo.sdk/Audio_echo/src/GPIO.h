/*
 * GPIO.h
 *
 *  Created on: Jul 9, 2017
 *      Author: enzoevers
 */

#ifndef SRC_GPIO_H_
#define SRC_GPIO_H_

#include "xgpio.h"
#include "xil_types.h"

u8 Init_GPIO();

u8 R_SWS();
void W_LEDS(u8 u8LedData);

#endif /* SRC_GPIO_H_ */
