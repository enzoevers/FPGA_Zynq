/*
 * I2C.h
 *
 *  Created on: Jul 9, 2017
 *      Author: enzoevers
 */

#ifndef SRC_I2C_H_
#define SRC_I2C_H_

//------------------------------------------//
//				Includes					//
//------------------------------------------//
#include "xil_types.h"
#include "xiicps.h"

//------------------------------------------//
//				Defines						//
//------------------------------------------//
// Slave address for the SSM audio controller
#define I2C_PS_0_ID 			XPAR_PS7_I2C_0_DEVICE_ID

// I2C Serial Clock frequency in Hertz
#define I2C_SCLK_RATE			100000

//------------------------------------------//
//				Variables					//
//------------------------------------------//
XIicPs I2C;

//------------------------------------------//
//				Prototype functions		 	//
//------------------------------------------//
u8 Init_I2C();
void W_I2C(u16 u16SlaveAddr, u8 u8RegAddr, u16 u16Data);


#endif /* SRC_I2C_H_ */
