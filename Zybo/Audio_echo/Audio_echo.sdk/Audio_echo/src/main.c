/*
 * main.c
 *
 *  Created on: Jul 9, 2017
 *      Author: enzoevers
 */


#include "I2C.h"
#include "GPIO.h"
#include "SSM2603.h"

//#include "xil_printf.h"

int main()
{
	//xil_printf("Initialization");
	Init_GPIO();
	Init_I2C();
	Init_SSM2603();
	//xil_printf("All initialized");

	//xil_printf("Enabling audio output");
	SSM2603_Enable();

	//xil_printf("Starting the stream");
	Stream_LineIn_HphOut();

	return 0;
}

