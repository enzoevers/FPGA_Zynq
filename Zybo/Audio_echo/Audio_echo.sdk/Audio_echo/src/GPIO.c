/*
 * GPIO.c
 *
 *  Created on: Jul 9, 2017
 *      Author: enzoevers
 */

#include "GPIO.h"
#include "xparameters.h"
#include "xstatus.h"

#define GPIO_SWS_ID 	XPAR_AXI_GPIO_1_DEVICE_ID
#define GPIO_LED_ID 	XPAR_AXI_GPIO_2_DEVICE_ID

XGpio SWS_inst;
#define SWS_CHANNEL 	1

XGpio LED_inst;
#define LED_CHANNEL 	1

u8 Init_GPIO()
{
	u8 status = XST_SUCCESS;

	// Switches
	status = XGpio_Initialize(&SWS_inst, GPIO_SWS_ID);
	if(status != XST_SUCCESS)
	{
		status = XST_FAILURE;
	}
	XGpio_SetDataDirection(&SWS_inst, SWS_CHANNEL, 0xF); // Set switches as input

	// Leds
	status = XGpio_Initialize(&LED_inst, GPIO_LED_ID);
	if(status != XST_SUCCESS)
	{
		status = XST_FAILURE;
	}
	XGpio_SetDataDirection(&LED_inst, LED_CHANNEL, 0x0); // Set leds as output

	return status;
}

u8 R_SWS()
{
	return XGpio_DiscreteRead(&SWS_inst, SWS_CHANNEL);
}

void W_LEDS(u8 u8LedData)
{
	XGpio_DiscreteWrite(&LED_inst, LED_CHANNEL, u8LedData);
}


